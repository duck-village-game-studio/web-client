import { useLocalStorage, writeStorage, deleteFromStorage } from '@rehooks/local-storage';

const AUTH_TOKEN_KEY = "auth-token";
export const getAuthToken = () => window.localStorage.getItem(AUTH_TOKEN_KEY);
export const setAuthToken = token => writeStorage(AUTH_TOKEN_KEY, token);
export const clearAuthToken = () => deleteFromStorage(AUTH_TOKEN_KEY);

export const useAuth = () => {
  const [token] = useLocalStorage(AUTH_TOKEN_KEY);
  return {
    isLoggedIn: !!token
  };
};

export default useAuth;
