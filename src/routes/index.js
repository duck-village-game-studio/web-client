import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import GuestRoute from "./GuestRoute";
import ProtectedRoute from "./ProtectedRoute";
import Homepage from "./homepage";
import Authentication from "./Authentication";
import styled from "styled-components";

const Router = styled(({ className }) => {
  return (
    <div className={className}>
      <Switch>
        <GuestRoute path="/authorize" component={Authentication} />
        <ProtectedRoute exact path="/" component={Homepage} />
        <Redirect to="/" />
      </Switch>
    </div>
  );
})`
  margin: 10px;
`;

export default Router;
