import * as React from "react";
import styled from "styled-components";
import { logout } from "./../../services/duckmain";

const ICON = {
  WOOD:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/wood.png",
  STONE:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/stone.png",
  GOLD:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/gold.png"
};

const Header = styled(({ className, gold = 0, wood = 0, stone = 0 }) => {
  return (
    <div className={className}>
      <section>
        <Resource icon={ICON.GOLD} value={gold} />
        <Resource icon={ICON.WOOD} value={wood} />
        <Resource icon={ICON.STONE} value={stone} />
      </section>
      <button onClick={() => logout()}>Logout</button>
    </div>
  );
})`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  & > section {
    display: flex;
    flex-direction: row;
  }
`;

const Resource = styled(({ className, icon, value }) => {
  return (
    <div className={className}>
      <img src={icon} />
      <span>{value}</span>
    </div>
  );
})`
  --size: 50px;
  img {
    width: var(--size);
    height: var(--size);
  }
`;

export default Header;
