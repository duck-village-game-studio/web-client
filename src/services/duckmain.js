import { setAuthToken, getAuthToken, clearAuthToken } from './../hooks/useAuth'

// const duckMainUrl = `http://${window.location.hostname}:3001`;
const duckMainUrl = `http://${window.location.hostname}/api`;

export const checkStatus = async () => {
  try {    
    console.debug('Checking server status...')
    await fetch(`${duckMainUrl}/status`);
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
};

export const login = async () => {
  try {
    console.debug('Logging into server...')
    const response = await fetch(`${duckMainUrl}/login`);
    const token = await response.json() 
    setAuthToken(token)
    return getAuthToken();
  } catch (error) {
    console.error(error)
  }
};

export const getUser = async () => {
    try {
      console.debug('Getting user info...')
      const user = await fetch(`${duckMainUrl}/village`);
      return user;
    } catch (error) {}
  };

export const logout = async () => {
    try {
      console.debug('Logging out...')
      clearAuthToken()
    } catch (error) {}
  };
