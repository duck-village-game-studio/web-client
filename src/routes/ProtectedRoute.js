import * as React from "react";
import { Route, Redirect } from "react-router-dom";
import useAuth from "./../hooks/useAuth";

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { isLoggedIn } = useAuth();
  const getProperComponent = props => {
    if (isLoggedIn) {
      return <Component {...rest} {...props} />;
    }
    return <Redirect to="/authorize" />;
  };

  return <Route {...rest} render={getProperComponent} />;
};

export default ProtectedRoute;
