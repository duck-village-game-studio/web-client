import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Router from './routes'
import BackendStatus from './components/BackendStatus'
import {
    BrowserRouter,
} from "react-router-dom";

ReactDOM.render(
    <>
        <BackendStatus />
        <BrowserRouter>
            <Router />
        </BrowserRouter>
    </>,
    document.getElementById('app'),
)
