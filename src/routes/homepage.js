import * as React from "react";
import styled from "styled-components";
import Header from "./../components/Header";

const BUILDINGS = {
  TOWN_HALL: "TOWN_HALL",
  STONE_MASON: "STONE_MASON",
  LUMBERJACK: "LUMBERJACK",
  GATE: "GATE",
  WAREHOUSE: "WAREHOUSE"
};

const IMAGE_URL = {
  TOWN_HALL:
    "https://storage.googleapis.com/duck-village-game-resources/assets/bdi_algeria_town_hall.gif",
  STONE_MASON:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/bdi_austria_art_depo.gif",
  LUMBERJACK:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/bdi_algeria_storehouse.gif",
  GATE:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/bdi_austria_gate.gif",
  WALL:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/bdi_austria_gate.gif",
  WAREHOUSE:
    "https://storage.cloud.google.com/duck-village-game-resources/assets/bdi_austria_market.gif",
  GRASS:
    "https://i.pinimg.com/originals/f3/92/2e/f3922e5e8d05771416593d07a25228c7.jpg"
};

const HomepageRoute = styled(({ className }) => {
  return (
    <>
      <Header />
      <section className={`${className} nes-container with-title`}>
        <Building name={BUILDINGS.TOWN_HALL} />
        <Building name={BUILDINGS.STONE_MASON} />
        <Building name={BUILDINGS.LUMBERJACK} />
        <Building name={BUILDINGS.GATE} />
        <Building name={BUILDINGS.WAREHOUSE} />
      </section>
    </>
  );
})`
  display: flex;
  justify-content: space-evenly;
  background-image: url(${IMAGE_URL.GRASS});
`;

const Building = styled(({ className, name }) => {
  return (
    <div className={className}>
      <img src={IMAGE_URL[name]} />
      <ToolBox building={name} />
    </div>
  );
})``;

const ToolBox = styled(({ className }) => {
  return (
    <section className={className}>
      <button>Upgrade</button>
    </section>
  );
})``;

export default HomepageRoute;
