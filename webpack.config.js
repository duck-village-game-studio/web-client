const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')

const config = {
  target: 'web',
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    main: [
      '@babel/polyfill',
      './src/index',
    ],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(t|j)s?$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: process.env.PORT,
    host: '0.0.0.0',
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  resolve: {
    extensions: ['.ts', '.js',],
  },
  stats: {
    colors: true
  },
}

module.exports = config
