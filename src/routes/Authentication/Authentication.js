import * as React from "react";
import styled from "styled-components";
import { login } from "./../../services/duckmain";

export const Authentication = React.memo(({ className }) => {
  return (
    <div className={`${className} nes-container with-title`}>
      <span>Auth</span>
      <div className="nes-field">
        <label>
          login
          <input type="text" className="nes-input" />
        </label>
      </div>
      <div className="nes-field">
        <label>
          password
          <input type="password" className="nes-input" />
        </label>
      </div>
      <br />
      <button
        onClick={() => login()}
        type="button"
        className="nes-btn is-primary"
      >
        Login
      </button>
    </div>
  );
});

export const styledComponent = styled(Authentication)``;

export default styledComponent;
