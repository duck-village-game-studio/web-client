## Start with Docker

```bash
docker build . -t duck-web-client
docker run -p 80:80 duck-web-client
```
## Manually deploy latest version
```bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/duck-village-game-studio/web-client .
docker push registry.gitlab.com/duck-village-game-studio/web-client
Test locally:
docker run -p 8080:80 registry.gitlab.com/duck-village-game-studio/web-client
```
## How to install

In order to install dependencies, you need Node.js. In order to install it, you can use [nvm](https://github.com/creationix/nvm). Recommended version is `12`

```bash
npm install
```

### Start development with Docker

This will start `docker` instance binded to port `3000` by default. You can change port with `PORT` environment variable.

```bash
docker-compose up --build
```