import * as React from 'react'
import styled from 'styled-components'
import { checkStatus } from './../../services/duckmain'

const DOT_STATUS = {
    'ENABLED': 'ENABLED',
    'DISABLED': 'DISABLED',
}
const PROBING_INTERVAL = 10 * 1000; // check status each 10 seconds

export const BackendStatus = React.memo(({ className }) => {
    const [status, setStatus] = React.useState(DOT_STATUS.DISABLED)
    React.useEffect(() => {
        const interval = setInterval(async () => {
            const result = await checkStatus()
            if(!result) {
                return setStatus(DOT_STATUS.DISABLED)
            }
            setStatus(DOT_STATUS.ENABLED)
        }, PROBING_INTERVAL)
        return () => clearInterval(interval)
    }, [])
    return (
        <div className={className}>
            <Dot status={status} />
        </div>
    )
})


export const Dot = styled(({ className }) => <div className={className}/>)`
    --size: 20px;
    width: var(--size);
    height: var(--size);
    background-color: ${({status}) => (
        status === DOT_STATUS.ENABLED 
        ? 'greenyellow' 
        : 'orangered'
    )};
    border-radius: 50%;
    border: 1px solid black;
`

export const styledComponent = styled(BackendStatus)`
    position: absolute;
    right: 10px;
    bottom: 10px;
`

export default styledComponent
