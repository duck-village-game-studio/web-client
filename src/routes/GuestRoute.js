import * as React from "react";
import * as PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import useAuth from "./../hooks/useAuth";

export const GuestRoute = React.memo(({ component: Component, ...rest }) => {
  const { isLoggedIn } = useAuth();
  const getProperComponent = props => {
    if (isLoggedIn) {
      return <Redirect to="/" />;
    }

    return <Component {...rest} {...props} />;
  };

  return <Route {...rest} render={getProperComponent} />;
});

GuestRoute.propTypes = {
  component: PropTypes.object.isRequired
};

export default GuestRoute;
